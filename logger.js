Object.defineProperty(exports, "__esModule", { value: true });
/** 全局变量 */
let g_logger = undefined;
function setLogger(logger) {
    g_logger = logger;
}
exports.default = {
    SetLogger: setLogger,
    trace: function (message, ...args) {
        if (g_logger && typeof (g_logger.trace) == "function") {
            g_logger.trace(message, ...args);
        }
        else {
            console.log(message, ...args);
        }
    },
    debug: function (message, ...args) {
        if (g_logger && typeof (g_logger.debug) == "function") {
            g_logger.debug(message, ...args);
        }
        else {
            console.debug(message, ...args);
        }
    },
    info: function (message, ...args) {
        if (g_logger && typeof (g_logger.info) == "function") {
            g_logger.info(message, ...args);
        }
        else {
            console.info(message, ...args);
        }
    },
    warn: function (message, ...args) {
        if (g_logger && typeof (g_logger.warn) == "function") {
            g_logger.warn(message, ...args);
        }
        else {
            console.warn(message, ...args);
        }
    },
    error: function (message, ...args) {
        if (g_logger && typeof (g_logger.error) == "function") {
            g_logger.error(message, ...args);
        }
        else {
            console.error(message, ...args);
        }
    },
    fatal: function (message, ...args) {
        if (g_logger && typeof (g_logger.fatal) == "function") {
            g_logger.fatal(message, ...args);
        }
        else {
            console.error(message, ...args);
        }
    },
};
